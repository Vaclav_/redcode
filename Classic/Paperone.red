;redcode-94
;name Paperone
;author Beppe Bezzi
;(modified for educational purposes by Csaba Biro)
;strategy Silk replicator
;kill Paperone
;assert CORESIZE == 8000

start   spl     1,      <300
        mov     -1,     0
        spl     1,      <150

silk    spl     3620,   #0      ;split to new copy
        mov.i   >-1,    }-1     ;copy self to new location
        mov.i   bomb,   >2005   ;linear bombing
        add.a   #50,    silk    ;distance new copy   
        jmp     silk,   <silk   ;reset source pointer, make new copy
bomb    dat.f   0,      0       ;bomb
