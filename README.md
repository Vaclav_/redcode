My programs in Redcode, open source programs written by others and utilities for Corewar game.
========
"Corewar is a game played between two or more computer programs on behalf of the players who create them. Known as warriors, these programs are written in Redcode, a low-level language similar to assembly. Warriors battle to eliminate all opponents in the core memory of the MARS virtual computer."
www.corewar.co.uk