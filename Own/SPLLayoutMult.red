;redcode-94
;name SPLLayout
;author Vaclav
;strategy 10xSPL Stunner, then clear
;assert CORESIZE == 8000

bsize	equ	6
step	equ	6
end_cnt	equ	(795-bsize)
cpystep	equ	800

org	start

bomb	SPL	0	,#123
cnt	DAT	#bsize	,#bsize	;A: cur period, B:inc size
startM	MOV	bomb	,>pos
	DJN.A	-1	,cnt
pos	ADD	u_end	,#10
	MOV.BA	cnt	,cnt
	SLT	#end_cnt,pos
u_end	JMP	startM	,step
	SPL	0	,#-1
	MOV	u_clr	,<-1
u_clr	DAT	$0	,$0
sep	DAT	$0	,$0

mov_p	DAT	#bomb	,#0		;index src offset, dest offset
mp_def	DAT	#bomb	,#1		;default copy indexes
start	MOV.A	mp_def	,mov_p		;reset index bounds
	MOV.B	offs	,mov_p
	MOV	}mov_p	,>mov_p		;self copy loop
	JMN.F	start+2	,*mov_p		;self copy loop
offs	ADD	#cpystep,#cpystep	;inc block count
	SLT	offs	,#700
spl_cnt	JMP	start	,#9
	
	ADD.BA	split,	split
split	SPL	(startM-mov_p+3),	#800
	DJN.B	-2	,spl_cnt

	JMP	startM
