;redcode-94b
;name Pt0
;author Vaclav
;strategy Stun bomber, clear
;assert CORESIZE == 8000

OFFSET	equ	191
STEP	equ	94
bomb1	equ	clr

org start


pos	ADD.AB	#STEP,	#OFFSET		;bombing location
start	MOV	bomb1,	>pos
	MOV	bomb2,	@pos
	JMN.A	pos,	start		;continue until pos is not bombed
	JMP	clr
	FOR 8
	DAT	$0,	$0
	ROF
clr	SPL	0,	#2		;clear
	MOV	1,	<(clr-1)
	DAT	$0,	$0
	DAT	$0,	$0		;these locations are
	DAT	$0,	$0		;going to be bombed
	DAT	$0,	$0
bomb2	MOV	-1,	>-1

end
