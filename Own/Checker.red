;redcode-94
;name Checker
;author Vaclav
;strategy Self-checking, self-recovering
;assert 1

step	equ	(CORESIZE/2+1)
mov_p	equ	(LAST+1) ;(mp_def-1)
CHKVAL	equ	4038

org	start

					;index src offset, dest offset
mp_def	DAT	#-LAST-1,#step-LAST-2	;default copy indexes
start	MOV	mp_def	,mov_p		;reset index bounds
cpyloop	MOV	}mov_p	,>mov_p		;self copy loop
	JMN.A	cpyloop	,mov_p		;self copy loop
	SPL	verify+step-1
;	NOP	0
verify	MOV	mp_def	,mov_p	;reset loop
	MOV.A	#0	,chksum
acc	ADD.BA	>mov_p	,chksum	;check
chksum	NOP	#123	,}mov_p
	JMN.A	acc	,mov_p
	SEQ.A	#CHKVAL	,chksum		;if counter == real checksum
	JMP	start		 	;start to copy if not OK
LAST	JMP	verify			;continue checking if ok
;DAT	0
;DAT	123,	123
