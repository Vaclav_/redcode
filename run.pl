#!/usr/bin/perl
use strict;
use warnings;

#usage: run.pl arg0
#	arg0: file to benchmark

#Settings
my $rounds = 100;
my $options = '-r '.$rounds.' -d 100 -b';

my $cmd;
my @cmd_output;

my $wins = 0;
my $losses = 0;
my $ties = 0;
my $games = 0;

my $total_score = 0;
my $avg_score;
my $round_ratio = ($rounds/100);

my @files = <Benchmark/Wilfiz/*.red>;
	foreach my $file (@files) {
	#don't play with self
	if( $ARGV[0] eq $file) {
		next;
	}
	$cmd = 'pmars.exe ' . $ARGV[0] . ' ' . $file . ' ' . $options;
	my @cmd_output = `$cmd`;
	print "Opponent: ".$file."\n";
	my $str_num = 0;
	foreach(@cmd_output) {
		$str_num++;
		if( $str_num == 1 ) {
			my @m_score = $_ =~ /(\d+)/g;
			#find the last number
			my $k;
			for( $k=0; defined($m_score[$k]); $k++) {}
			print "Score: ".$m_score[$k-1]/$round_ratio."\n";
			$total_score += $m_score[$k-1];
		} elsif( $_ =~ /^Results/ ) {
			my @stats = $_ =~ /(\d+)/g;
			print "W ".$stats[0]/$round_ratio." |L ".$stats[1]/$round_ratio." T| ".$stats[2]/$round_ratio."\n";
			$wins += $stats[0];
			$losses += $stats[1];
			$ties += $stats[2];
			$games++;
		}
	}
}

$wins /= $games;
$wins /= $round_ratio;
$losses /= $games;
$losses /= $round_ratio;
$ties /= $games;
$ties /= $round_ratio;
$avg_score = $total_score / $games;
$avg_score /= $round_ratio;
print 'Games: ' . $games . " * " . $rounds . "\n\n";
printf "Average Wins: \%6.2f Losses: \%6.2f Ties: \%6.2f\n\n", $wins, $losses, $ties;
printf "Average Score: \%6.2f\n", $avg_score;



